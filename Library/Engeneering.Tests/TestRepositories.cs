﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Repositories;
using Library;

namespace Engeneering.Tests
{
    class TestRepository : IRepository
    {
        private List<Diagnostic> diagnostics;
        private List<Notification> notifications;

        public TestRepository()
        {
            diagnostics = new List<Diagnostic>();
            notifications = new List<Notification>();
        }
        
        public IReadOnlyCollection<Diagnostic> Diagnostics { get { return diagnostics; } }
        public IReadOnlyCollection<Notification> Notifications { get { return notifications; } }

        void IRepository.AddNotification(int turbineId, string message) { }

        public void AddNotificationTest(Notification notification)
        {
            notifications.Add(notification);
        }

        public void AddDiagnostic(Diagnostic diagnostic)
        {
            diagnostics.Add(diagnostic);
        }
    }

    class TestBrigadesRepository : IBrigadesRepository
    {
        private List<Brigade> brigades = new List<Brigade>();

        IReadOnlyCollection<Brigade> IBrigadesRepository.Brigades
        {
            get
            {
                return brigades;
            }
        }

        void IBrigadesRepository.AddBrigade(Brigade brigade)
        {
            brigades.Add(brigade);
        }

        void IBrigadesRepository.UpdateBrigade(Brigade brigade)
        {

        }
    }

    class TestExpeditionsRepository : IExpeditionsRepository
    {
        private List<Expedition> expeditions = new List<Expedition>();

        IReadOnlyCollection<Expedition> IExpeditionsRepository.Expeditions
        {
            get
            {
                return expeditions;
            }
        }

        void IExpeditionsRepository.AddExpedition(Expedition expedition)
        {
            expeditions.Add(expedition);
        }

        void IExpeditionsRepository.UpdateExpedition(Expedition expedition)
        {
        }
    }
}
