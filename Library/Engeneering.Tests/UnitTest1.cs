﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library;
using Library.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Engeneering.Tests
{
    [TestClass]
    public class UnitTest1
    {
        //тест-кейс 0
        //Получение списка турбин.
        //Просмотр информации о турбине
        [TestMethod]
        public void TestTurbinesList()
        {
            var ps = new TestPersonSystem();
            var ts = new TestTurbinesSystem();
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            Turbine turbine1 = new Turbine(1, "tutu", "t1");
            Turbine turbine2 = new Turbine(2, "tata", "t2");
            ts.Turbines = new List<Turbine>() { turbine1, turbine2 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            Assert.AreEqual(2, s.Turbines.Count());
            Assert.AreEqual(1, s.Turbines.OrderBy(x => x.ID).ElementAt(0).ID);
            Assert.AreEqual("t1", s.Turbines.OrderBy(x => x.ID).ElementAt(0).Name);
            Assert.AreEqual("tutu", s.Turbines.OrderBy(x => x.ID).ElementAt(0).Definition);
            Assert.AreEqual(2, s.Turbines.OrderBy(x => x.ID).ElementAt(1).ID);
            Assert.AreEqual("t2", s.Turbines.OrderBy(x => x.ID).ElementAt(1).Name);
            Assert.AreEqual("tata", s.Turbines.OrderBy(x => x.ID).ElementAt(1).Definition);
        }

        //тест-кейс 1. 
        //Проверка наличия сигнала тревоги.
        //Проверка наличия уведомления о происшествии на турбине.
        //Закрытие уведомления без экспедиции.
        //Назначение экспедиции на уведомление.
        //Закрытие уведомления.
        //Попытка повторного закрытия уведомления.
        //Попытка повторного назначения.
        //Проверка отсутствия сигнала тревоги.
        [TestMethod]
        public void TestAlert()
        {
            var ps = new TestPersonSystem();
            var ts = new TestTurbinesSystem();
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();

            Turbine turbine = new Turbine(1, "", "t1");
            ts.Turbines = new List<Turbine>() { turbine };
            rr.AddNotificationTest(new Notification(turbine, 1, "alart!!1"));
            rr.AddNotificationTest(new Notification(turbine, 2, "alart!!2"));     
            Brigade b = new Brigade(1, new SeniorEngineer(1, "ivan"));
            var s = new ServiceSystem(ts, ps, rr, er, br);
            //проверка 
            Assert.IsTrue(s.Alert);
            Assert.AreEqual(2, s.GetOpenedNotifications().Count());
            Assert.AreEqual(1, s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).ID);
            Assert.AreEqual(2, s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(1).ID);
            s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).Close();
            Assert.AreEqual(1, s.GetOpenedNotifications().Count());
            s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).SetExpedition(new Expedition(1, null, false, DateTime.Now));
            s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).Close();
            Assert.AreEqual(0, s.GetOpenedNotifications().Count());
            try
            {
                s.Notifications.OrderBy(x => x.ID).First().Close();
                Assert.Fail();
            }
            catch (Exception) { }
            try
            {
                s.Notifications.OrderBy(x => x.ID).First().SetExpedition(new Expedition(1, null, false, DateTime.Now));
                Assert.Fail();
            }
            catch (Exception) { }
            Assert.IsFalse(s.Alert);
        }

        //тест-кейс 2
        //Назначение будущих выездов.
        //Просмотр запланированных выездов.
        [TestMethod]
        public void TestFutureExpedition()
        {
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            Brigade b2 = new Brigade(1, p1);
            b1.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "turr", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b1);
            //успешное назначение бригады
            var e1 = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10, 10, 1, 23)); //повторяющееся и в будущем
            var e2 = new Expedition(2, turbine1, true, new DateTime(2010, 10, 10, 10, 1, 23)); //повторяющееся и в прошлом
            var e3 = new Expedition(3, turbine1, false, new DateTime(2020, 10, 10, 10, 1, 23)); //разовая и в будущем
            var e4 = new Expedition(4, turbine1, false, new DateTime(2010, 10, 10, 10, 1, 23)); //разовая и в прошлом
            e1.SetBrigade(b1);
            s.AddExpedition(e1);
            e2.SetBrigade(b1);
            s.AddExpedition(e2);
            e3.SetBrigade(b1);
            s.AddExpedition(e3);
            e4.SetBrigade(b1);
            s.AddExpedition(e4);
            //все повторяющееся являются запланированными + разовые из будущего
            Assert.AreEqual(3, s.GetFutureExpeditions().Count());
            var exp = s.GetFutureExpeditions().OrderBy(x => x.ID);
            Assert.AreEqual(1, exp.ElementAt(0).ID);
            Assert.AreEqual(2, exp.ElementAt(1).ID);
            Assert.AreEqual(3, exp.ElementAt(2).ID);
        }

        //тест-кейс 3
        //Проведение диагностики.
        //Проверка цикла выполнения цикла состояний экспедиций.
        //Добавление тестов в экспедицию.
        //Добавление результатов тестов в экспедицию.
        [TestMethod]
        public void TestDiagnostic()
        {
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            Brigade b2 = new Brigade(1, p1);
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            b1.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "turr", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b1);
            //выполнили 
            var e1 = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10, 10, 1, 23));
            e1.SetBrigade(b1);
            s.AddExpedition(e1);
            e1.AddTest(new ExpeditionTest(1, new TestTask(1, "taskname", "def", TestTask.СriticalityDimension.Important)));
            e1.AddTest(new ExpeditionTest(2, new TestTask(2, "taskname2", "def", TestTask.СriticalityDimension.Important)));
            Assert.IsFalse(e1.IsAccepted);
            e1.Accept();
            Assert.IsTrue(e1.IsAccepted);
            Assert.AreEqual(Expedition.ExpeditionStates.NotBusy, e1.CurrentState);
            e1.NextStep();
            Assert.AreEqual(Expedition.ExpeditionStates.Departured, e1.CurrentState);
            e1.NextStep();
            Assert.AreEqual(Expedition.ExpeditionStates.Started, e1.CurrentState);
            e1.Tests.First().StartTest();
            e1.Tests.First().EndTest(TestTask.TestResult.Fail);
            e1.Tests.Last().StartTest();
            e1.Tests.Last().EndTest(TestTask.TestResult.Fail);
            e1.NextStep();
            Assert.AreEqual(Expedition.ExpeditionStates.Ended, e1.CurrentState);
            e1.NextStep();
            Assert.AreEqual(Expedition.ExpeditionStates.NotBusy, e1.CurrentState);
            s.AddDiagnostic(e1);
            Assert.AreEqual(s.Diagnostics.First().Date, e1.DateEnd);
            Assert.AreEqual(2, s.Diagnostics.First().Tests.Count());
            Assert.AreEqual(1, s.Diagnostics.First().Turbine.ID);
        }

        //тест-кейс 4
        //Добавление инженера в бригаду.
        //Удаление инженера из бригады.
        //Получение списка инженеров в бригаде.
        //Получение общего списка инженеров.
        [TestMethod]
        public void TestEngineerDelete()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            var p3 = new JuniorEngineer(3, "p3");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            Assert.AreEqual(0, b1.Persons.Count());
            b1.AddPerson(p2);
            Assert.AreEqual(1, b1.Persons.Count());
            b1.AddPerson(p3);
            Assert.AreEqual(2, b1.Persons.Count());
            b1.RemovePerson(p3);
            Assert.AreEqual(1, b1.Persons.Count());
            var s = new ServiceSystem(new TestTurbinesSystem(), ps, rr, er, br);
            Assert.AreEqual(2, s.Engineers.Count());
        }

        //тест-кейс 5
        //Утверждение на выезд
        [TestMethod]
        public void TestAcceptExpediton()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var ts = new TestTurbinesSystem();
            Turbine turbine = new Turbine(1, "", "t1");
            Brigade b = new Brigade(1, new SeniorEngineer(1, "ivan"));
            ts.Turbines = new List<Turbine>() { turbine };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddNotification(1, "");
            rr.AddNotificationTest(new Notification(turbine, 1, ""));
            Assert.IsTrue(s.Alert);
            s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).SetExpedition(new Expedition(1, null, false, DateTime.Now));
            s.GetOpenedNotifications().OrderBy(x => x.ID).ElementAt(0).Close();
            Assert.AreEqual(0, s.GetOpenedNotifications().Count());
            try
            {
                s.Notifications.OrderBy(x => x.ID).First().Close();
                Assert.Fail();
            }
            catch (Exception) { }
            try
            {
                s.Notifications.OrderBy(x => x.ID).First().SetExpedition(new Expedition(1, null, false, DateTime.Now));
                Assert.Fail();
            }
            catch (Exception) { }
            Assert.IsFalse(s.Alert);
        }

        //тест-кейс 6
        //Утверждение состава
        [TestMethod]
        public void TestAcceptStaffExpediton()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b2 = new Brigade(1, p1);
            b2.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b2);
            //успешное назначение бригады
            var e = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10));
            e.SetBrigade(b2);
            s.AddExpedition(e);
            Assert.IsFalse(s.Expeditions.First().IsAccepted);
            s.Expeditions.First().Accept();
            Assert.IsTrue(s.Expeditions.First().IsAccepted);
        }

        //тест-кейс 7
        //Получение списка выехавших бригад
        [TestMethod]
        public void TestCurrentExpedtion()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            Brigade b2 = new Brigade(1, p1);
            b1.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "turr", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b1);
            //выполнили 
            var e1 = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10, 10, 1, 23));
            e1.SetBrigade(b1);
            s.AddExpedition(e1);
            e1.AddTest(new ExpeditionTest(1, new TestTask(1, "taskname", "def", TestTask.СriticalityDimension.Important)));
            e1.AddTest(new ExpeditionTest(2, new TestTask(2, "taskname2", "def", TestTask.СriticalityDimension.Important)));
            Assert.IsFalse(e1.IsAccepted);
            e1.Accept();
            Assert.IsTrue(e1.IsAccepted);
            Assert.AreEqual(Expedition.ExpeditionStates.NotBusy, e1.CurrentState);
            e1.NextStep();
            Assert.IsNotNull(e1.DateDeparture);
            Assert.IsTrue(e1.DateDeparture <= DateTime.Now);
            Assert.IsNull(e1.DateEnd);
            Assert.AreEqual(1, s.GetCurrentExpeditions().Count());
            Assert.AreEqual(1, s.GetCurrentExpeditions().First().ID);
        }

        //тест-кейс 8
        //Просмотр результатов диагностики
        [TestMethod]
        public void TestDiagnosticResult()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            b1.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "turr", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b1);
            //выполнили 
            var e1 = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10, 10, 1, 23));
            e1.SetBrigade(b1);
            s.AddExpedition(e1);
            e1.AddTest(new ExpeditionTest(1, new TestTask(1, "taskname", "def", TestTask.СriticalityDimension.Important)));
            e1.AddTest(new ExpeditionTest(2, new TestTask(2, "taskname2", "def", TestTask.СriticalityDimension.Important)));
            Assert.IsFalse(e1.IsAccepted);
            e1.Accept();
            Assert.IsTrue(e1.IsAccepted);
            e1.NextStep();
            e1.NextStep();
            e1.Tests.First().StartTest();
            e1.Tests.First().EndTest(TestTask.TestResult.Fail);
            e1.Tests.Last().StartTest();
            e1.Tests.Last().EndTest(TestTask.TestResult.Fail);
            e1.NextStep();
            e1.NextStep();
            Assert.AreEqual(Expedition.ExpeditionStates.NotBusy, e1.CurrentState);
            s.AddDiagnostic(e1);
            Assert.AreEqual(s.Diagnostics.First().Date, e1.DateEnd);
            Assert.AreEqual(2, s.Diagnostics.First().Tests.Count());
            Assert.AreEqual(1, s.Diagnostics.First().Turbine.ID);
        }

        //тест-кейс 9
        //Планирование планового обслуживания
        [TestMethod]
        public void TestPlanExpediton()
        {
            var rr = new TestRepository();
            var br = new TestBrigadesRepository();
            var er = new TestExpeditionsRepository();
            var ps = new TestPersonSystem();
            var p1 = new SeniorEngineer(1, "p1");
            var p2 = new JuniorEngineer(2, "p2");
            ps.Person = new List<Person>() { p1, p2 };
            Brigade b1 = new Brigade(1, p1);
            b1.AddPerson(p2);
            var ts = new TestTurbinesSystem();
            Turbine turbine1 = new Turbine(1, "", "t1");
            ts.Turbines = new List<Turbine>() { turbine1 };
            var s = new ServiceSystem(ts, ps, rr, er, br);
            s.AddBrigade(b1);
            //успешное назначение бригады
            Expedition e = new Expedition(1, turbine1, true, new DateTime(2020, 10, 10));
            e.SetBrigade(b1);
            s.AddExpedition(e);
            Assert.IsTrue(e.IsRepeated);
            Assert.IsNotNull(e.Period);
        }
    }
}
