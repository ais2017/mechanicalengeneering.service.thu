﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;
using Library.Repositories;

namespace Engeneering.Tests
{
    //мок-класс
    public class TestTurbinesSystem : ITurbinesSystem
    {
        public IReadOnlyCollection<Turbine> Turbines { get; set; }
    }
}
