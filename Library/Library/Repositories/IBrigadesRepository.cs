﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public interface IBrigadesRepository
    {
        /// <summary>
        /// Список бригад
        /// </summary>
        IReadOnlyCollection<Brigade> Brigades { get; }

        /// <summary>
        /// Добавить бригаду
        /// </summary>
        /// <param name="brigade">Бригада</param>
        void AddBrigade(Brigade brigade);

        /// <summary>
        /// Обновить бригаду
        /// </summary>
        /// <param name="brigade">Бригада</param>
        void UpdateBrigade(Brigade brigade);
    }
}
