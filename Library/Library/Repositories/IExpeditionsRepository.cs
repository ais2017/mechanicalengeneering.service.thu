﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    /// <summary>
    /// Интерфейс для работы с выездами
    /// </summary>
    public interface IExpeditionsRepository
    {
        /// <summary>
        /// Список экспедиций
        /// </summary>
        IReadOnlyCollection<Expedition> Expeditions { get; }

        /// <summary>
        /// Добавить экспедицию
        /// </summary>
        /// <param name="expedition">Экспедиция</param>
        void AddExpedition(Expedition expedition);

        /// <summary>
        /// Изменение экспедиции
        /// </summary>
        /// <param name="expedition"></param>
        void UpdateExpedition(Expedition expedition);
    }
}
