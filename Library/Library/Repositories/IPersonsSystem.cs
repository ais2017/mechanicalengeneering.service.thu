﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    /// <summary>
    /// Интерфейс внешней системы учёта персонала 
    /// </summary>
    public interface IPersonsSystem
    {
        /// <summary>
        /// Список людей - работников
        /// </summary>
        IReadOnlyCollection<Person> Person { get; }
    }
}
