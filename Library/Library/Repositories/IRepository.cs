﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    /// <summary>
    /// Интерфейс репозитория
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Список диагностик
        /// </summary>
        IReadOnlyCollection<Diagnostic> Diagnostics { get; }
        /// <summary>
        /// Список уведомлений
        /// </summary>
        IReadOnlyCollection<Notification> Notifications { get; }
        
        /// <summary>
        /// Добавлене уведомления
        /// </summary>
        /// <param name="turbineId">Идентификатор турбины</param>
        /// <param name="message">Сообщение</param>
        void AddNotification(int turbineId, string message);

        /// <summary>
        /// Добавить результаты диагностики
        /// </summary>
        /// <param name="diagnostic">Диагностика</param>
        void AddDiagnostic(Diagnostic diagnostic);
    }
}
