﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    /// <summary>
    /// Интерфейс внешней системы учйта турбин
    /// </summary>
    public interface ITurbinesSystem
    {
        /// <summary>
        /// Список турбин
        /// </summary>
        IReadOnlyCollection<Turbine> Turbines { get; }
    }
}
