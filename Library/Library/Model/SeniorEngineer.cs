﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Старший инженер
    /// </summary>
    public class SeniorEngineer : JuniorEngineer
    {
        /// <summary>
        /// Создание старшего инденера
        /// </summary>
        /// <param name="id">Униклаьный идентификатор работника</param>
        /// <param name="name">Имя работника</param>
        public SeniorEngineer(int id, string name) : base(id, name){ }
    }
}
