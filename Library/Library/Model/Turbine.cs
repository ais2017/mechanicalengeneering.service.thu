﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Класс турбины, получаемый из внешней системы
    /// </summary>
    public class Turbine
    {
        /// <summary>
        /// Уникальный id турбины
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Описание турбины
        /// </summary>
        public string Definition { get; private set; }
        /// <summary>
        /// Название турбины
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Создание турбины
        /// </summary>
        /// <param name="id">Уникальный идентификатор</param>
        /// <param name="definition">Описание турбины</param>
        /// <param name="name">Название турбины</param>
        public Turbine(int id, string definition, string name)
        {
            ID = id;
            Definition = definition;
            Name = name;
        }
    }
}
