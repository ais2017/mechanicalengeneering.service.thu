﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Бригада из бригадира и не более чем пяти подчинённых
    /// </summary>
    public class Brigade
    {
        /// <summary>
        /// Уникальный id бригады
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Рядовой состав бригады
        /// </summary>
        public List<JuniorEngineer> Persons { get; private set; }
        /// <summary>
        /// Бригадир бригады
        /// </summary>
        public SeniorEngineer Brigadier { get; private set; }
        /// <summary>
        /// Бит, утверждены или нет изменения в бригаде
        /// </summary>
        public bool Checked { get; set; }

        /// <summary>
        /// Создание бригады
        /// </summary>
        /// <param name="id">Уникальный id бригады</param>
        /// <param name="brigadier">Бригадир (не меняется)</param>
        public Brigade(int id, SeniorEngineer brigadier)
        {
            ID = id;
            Brigadier = brigadier;
            Persons = new List<JuniorEngineer>();
            Checked = true;
        }

        /// <summary>
        /// Добавление человека в бригаду
        /// </summary>
        /// <param name="person">Добавляемый человек</param>
        public void AddPerson(JuniorEngineer person)
        {
            if (Persons.Count() == 5)
                throw new Exception("too many people");
            if (Persons.Exists(x => x.ID == person.ID))
                throw new Exception("human exist in brigade");
            Persons.Add(person);
            Checked = false;
        }

        /// <summary>
        /// Удаление человека из бригады
        /// </summary>
        /// <param name="person">Удаляемый человек</param>
        public void RemovePerson(JuniorEngineer person)
        {
            if (!Persons.Exists(x => x.ID == person.ID))
                throw new Exception("human not found");
            Persons.RemoveAll(x => x.ID == person.ID);
            Checked = false;
        }           
    }
}
