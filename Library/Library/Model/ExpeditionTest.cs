﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Запись с информацией о тесте и его результатах
    /// </summary>
    public class ExpeditionTest
    {
        /// <summary>
        /// Уникальный id записи
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Задание теста
        /// </summary>
        public TestTask Task { get; private set; }
        /// <summary>
        /// Время начала теста
        /// </summary>
        public DateTime? DateStart { get; private set; }
        /// <summary>
        /// Время окончания теста
        /// </summary>
        public DateTime? DateEnd { get; private set; }
        /// <summary>
        /// Результат выполнения теста
        /// </summary>
        public TestTask.TestResult Result { get; private set; } 

        /// <summary>
        /// Создание записи с тестом
        /// </summary>
        /// <param name="id">Уникальный идентификатор</param>
        /// <param name="task">Задание для теста</param>
        public ExpeditionTest(int id, TestTask task)
        {
            ID = id;
            Task = task;
            DateStart = null;
            DateEnd = null;
        }

        /// <summary>
        /// Запуск теста
        /// </summary>
        public void StartTest()
        {
            if (DateStart != null)
                throw new Exception("test started yet");
            DateStart = DateTime.Now;
        }

        /// <summary>
        /// Завершение теста 
        /// </summary>
        /// <param name="result">Результат выполнения теста</param>
        public void EndTest(TestTask.TestResult result)
        {
            if (DateEnd != null)
                throw new Exception("test ended yet");
            DateEnd = DateTime.Now;
            Result = result;
        }
    }
}
