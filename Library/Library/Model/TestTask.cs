﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Задания для теста
    /// </summary>
    public class TestTask
    {
        /// <summary>
        /// Степень критичность (критично или не важно)
        /// </summary>
        public enum СriticalityDimension { Important, Secondary };

        /// <summary>
        /// Возможные результаты задания (используется в других классах)
        /// </summary>
        public enum TestResult { Success, Fail };

        /// <summary>
        /// Уникальный id задания теста
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Имя теста
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Описание задания
        /// </summary>
        public string Definition { get; private set; }
        /// <summary>
        /// Критичность теста (определяет в какие группу может попасть тест)
        /// </summary>
        public СriticalityDimension Criticality { get; private set; }

        /// <summary>
        /// Конструктор теста, принимает основные характеристики
        /// </summary>
        /// <param name="id">Уникальный идентификатор</param>
        /// <param name="name">Имя теста</param>
        /// <param name="definition">Описание задания</param>
        /// <param name="criticality">Критичность</param>
        public TestTask(int id, string name, string definition, СriticalityDimension criticality)
        {
            ID = id;
            Name = name;
            Definition = definition;
            Criticality = criticality;
        }

        /// <summary>
        /// Изменение описание теста
        /// </summary>
        /// <param name="definition">Новое описание теста</param>
        public void ChangeDefinition(string definition)
        {
            Definition = definition;
        }

        /// <summary>
        /// Изменение критичности теста
        /// </summary>
        /// <param name="criticality">Новый уровень критичности</param>
        public void ChangeCriticality(СriticalityDimension criticality)
        {
            Criticality = criticality;
        }
    }
}
