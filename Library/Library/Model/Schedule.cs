﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Расписание необходимых посещений турбин
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// Набор расписаний для каждой из турбин
        /// </summary>
        public Dictionary<Turbine, List<Row>> Turbines { get; private set; }

        /// <summary>
        /// Создания расписания турбин
        /// </summary>
        public Schedule()
        {
            Turbines = new Dictionary<Turbine, List<Row>>();
        }
        /// <summary>
        /// Добавление турбины в расписание
        /// </summary>
        /// <param name="turbine"></param>
        public List<Row> AddTurbine(Turbine turbine)
        {
            if (Turbines.Count(x => x.Key.ID == turbine.ID) == 0)
                Turbines.Add(turbine, new List<Row>());
            return Turbines[turbine];
        }
        /// <summary>
        /// Добавление записи в расписание
        /// </summary>
        /// <param name="turbine">Турбина</param>
        /// <param name="row">Запись по турбине</param>
        public void AddRow(Turbine turbine, Row row)
        {
            if (Turbines.Count(x => x.Key.ID == turbine.ID) == 0)
                Turbines.Add(turbine, new List<Row>());
            Turbines[turbine].Add(row);
        }
    }
    /// <summary>
    /// Запись в расписании
    /// </summary>
    public class Row
    {
        /// <summary>
        /// Дата записи
        /// </summary>
        public DateTime Date { get; private set; }
        /// <summary>
        /// Экспедиция в расписании
        /// </summary>
        public Expedition Expedition { get; private set; }

        /// <summary>
        /// Создание строки
        /// </summary>
        /// <param name="date">Запланированная дата</param>
        public Row(DateTime date)
        {
            Date = date;
        }

        /// <summary>
        /// Назначить экспедицию
        /// </summary>
        /// <param name="expedition">Назанченная экспедиция</param>
        public void SetExpediotion(Expedition expedition)
        {
            Expedition = expedition;
        }
    }
}
