﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Младший инженер
    /// </summary>
    public class JuniorEngineer : Person
    {
        /// <summary>
        /// Создание младшего инженера
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <param name="name">Имя работника</param>
        public JuniorEngineer(int id, string name):base(id, name) {}
    }
}
