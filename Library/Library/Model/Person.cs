﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Работник
    /// </summary>
    public abstract class Person
    {
        /// <summary>
        /// Уникальный идентификатор работника
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Имя работника
        /// </summary>
        public string Name { get; private set; }
        
        /// <summary>
        /// Создание работника
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <param name="name">Имя работника</param>
        public Person(int id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
