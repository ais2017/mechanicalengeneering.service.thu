﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Repositories;

namespace Library
{
    /// <summary>
    /// Система обслуживания турбин
    /// </summary>
    public class ServiceSystem
    {
        /// <summary>
        /// Список бригад
        /// </summary>
        public IReadOnlyCollection<Brigade> Brigades { get { return BrigadesRepository.Brigades; } }
        /// <summary>
        /// Список выездов
        /// </summary>
        public IReadOnlyCollection<Expedition> Expeditions { get { return ExpeditionsRepository.Expeditions; } }
        /// <summary>
        /// Список уведомлений. Предназначен для хранения и добавления новых уведомлений 
        /// </summary>
        public IReadOnlyCollection<Notification> Notifications { get { return Repository.Notifications; } }
        /// <summary>
        /// Список диагностики
        /// </summary>
        public IReadOnlyCollection<Diagnostic> Diagnostics { get { return Repository.Diagnostics; } }
        /// <summary>
        /// Получение списка турбин
        /// </summary>
        public IReadOnlyCollection<Turbine> Turbines { get { return TurbineSystem.Turbines; } }
        /// <summary>
        /// Сигнал о тревоге - наличие незакрытых уведомлений о тревоге
        /// </summary>
        public bool Alert { get { return GetOpenedNotifications().Count() > 0; } }
        /// <summary>
        /// Получение списка инженеров
        /// </summary>
        public IReadOnlyCollection<JuniorEngineer> Engineers { get { return PersonSystem.Person.Where(x => x is JuniorEngineer).Cast<JuniorEngineer>().ToArray(); } }
        /// <summary>
        /// Интерфейс внешней системы учёта турбин
        /// </summary>
        private ITurbinesSystem TurbineSystem { get; set; }
        /// <summary>
        /// Интерфейс внешней системы учёта персон
        /// </summary>
        private IPersonsSystem PersonSystem { get; set; }
        /// <summary>
        /// Интерфейс хранилища
        /// </summary>
        private IRepository Repository { get; set; }
        /// <summary>
        /// Интерфейс хранилища для работы с выездами
        /// </summary>
        private IExpeditionsRepository ExpeditionsRepository { get; set; }
        /// <summary>
        /// Интерфейс хранилища для работы с бригадами
        /// </summary>
        private IBrigadesRepository BrigadesRepository { get; set; }

        /// <summary>
        /// Инициализация системы обслуживания турбин
        /// </summary>
        /// <param name="turbinesSystem">Система, реализующая интерфейс системы учёта турбин</param>
        /// <param name="personSystem">Система, реализующая интерфейс системы учёта персонала</param>
        /// <param name="repository">Интерфейс хранилища данных</param>
        /// <param name="expeditionsRepository">Интерфейс хранилища с выездами</param>
        /// <param name="brigadesRepository">Интерфейс хранилища с бригадами</param>
        public ServiceSystem(ITurbinesSystem turbinesSystem, IPersonsSystem personSystem, IRepository repository, 
            IExpeditionsRepository expeditionsRepository, IBrigadesRepository brigadesRepository)
        {
            TurbineSystem = turbinesSystem;
            PersonSystem = personSystem;
            Repository = repository;
            ExpeditionsRepository = expeditionsRepository;
            BrigadesRepository = brigadesRepository;
        }

        /// <summary>
        /// Добавить экспедицию
        /// </summary>
        /// <param name="expedition">Новая экспедиция</param>
        public void AddExpedition(Expedition expedition)
        {
            if (Brigades.Count(x => x.ID == expedition.Brigade.ID) == 0
                || TurbineSystem.Turbines.Count(x => x.ID == expedition.Turbine.ID) == 0)
                throw new Exception("arguments bad");
            ExpeditionsRepository.AddExpedition(expedition);
        }

        /// <summary>
        /// Обновить информацию об экспедиции
        /// </summary>
        /// <param name="expedition">Измененная экспедиция экспедиция</param>
        public void UpdateExpedition(Expedition expedition)
        {
            if (Brigades.Count(x => x.ID == expedition.Brigade.ID) == 0
                || TurbineSystem.Turbines.Count(x => x.ID == expedition.Turbine.ID) == 0
                || Expeditions.Count(x => x.ID == expedition.ID) == 0)
                throw new Exception("arguments bad");
            ExpeditionsRepository.UpdateExpedition(expedition);
        }
        
        /// <summary>
        /// Добавить новую бригаду в систему
        /// </summary>
        /// <param name="brigade">Новая бригада</param>
        public void AddBrigade(Brigade brigade)
        {
            BrigadesRepository.AddBrigade(brigade);
        }

        /// <summary>
        /// Обновить бригаду в системе
        /// </summary>
        /// <param name="brigade">Новая бригада</param>
        public void UpdateBrigade(Brigade brigade)
        {
            if (Brigades.Count(x => x.ID == brigade.ID) == 0)
                throw new Exception("brigade not found!");
            BrigadesRepository.AddBrigade(brigade);
        }

        /// <summary>
        /// Добавить новое уведомление 
        /// </summary>
        /// <param name="turbineId"> Идентификатор турбины</param>
        /// <param name="message"> Сообщение уведомления</param>
        public void AddNotification(int turbineId, string message)
        {
            if (Turbines.Count(x => x.ID == turbineId) == 0)
                throw new Exception("turbine not found!");
            Repository.AddNotification(turbineId, message);
        }

        /// <summary>
        /// Получить все открытые уведомления
        /// </summary>
        public IReadOnlyCollection<Notification> GetOpenedNotifications()
        {
            return Notifications.Where(x => x.State == Notification.NotificationStates.Opened).ToArray();
        }

        /// <summary>
        /// Добавление записи в диагностику по результатам жкспедиции
        /// </summary>
        /// <param name="expedition">Экспедиция</param>
        public void AddDiagnostic(Expedition expedition)
        {
            if (expedition.CurrentState != Expedition.ExpeditionStates.Ended && expedition.CurrentState != Expedition.ExpeditionStates.NotBusy)
                throw new Exception("expedition not ended!");
            if (Diagnostics.Count(x => x.Date == expedition.DateEnd && x.Turbine == expedition.Turbine) != 0)
                throw new Exception("diagnostic existed!");
            Repository.AddDiagnostic(new Diagnostic(expedition.Turbine, expedition.DateEnd, expedition.Tests));
        }

        /// <summary>
        /// Получение списка будущих экспедиций
        /// </summary>
        /// <returns>Список экспедиций</returns>
        public IEnumerable<Expedition> GetFutureExpeditions()
        {
            return Expeditions.Where(x => x.IsRepeated || x.PlannedStartDate > DateTime.Now);
        }

        /// <summary>
        /// Получение списка выехавших экспедиций
        /// </summary>
        /// <returns>Список выехавших экспедиций</returns>
        public IEnumerable<Expedition> GetCurrentExpeditions()
        {
            return Expeditions.Where(x => x.DateDeparture != null && x.DateDeparture < DateTime.Now && x.DateEnd == null);
        }
    }
}
