﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Запись о диагностике
    /// </summary>
    public class Diagnostic
    {
        /// <summary>
        /// Турбина, на которой проводилась диагностика
        /// </summary>
        public Turbine Turbine { get; private set; }
        /// <summary>
        /// Дата когда проводилась
        /// </summary>
        public DateTime? Date { get; private set; }
        /// <summary>
        /// Тесты, которые проводились
        /// </summary>
        public List<ExpeditionTest> Tests { get; private set; }

        /// <summary>
        /// Создание записи в диагностике
        /// </summary>
        /// <param name="turbine">Турбина</param>
        /// <param name="date">Дата</param>
        /// <param name="tests">Тесты</param>
        public Diagnostic(Turbine turbine, DateTime? date, List<ExpeditionTest> tests)
        {
            Turbine = turbine;
            Date = date;
            Tests = new List<ExpeditionTest>(tests);
        }

    }


}
