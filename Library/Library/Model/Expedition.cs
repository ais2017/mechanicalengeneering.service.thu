﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Класс экспедиции
    /// </summary>
    public class Expedition
    {
        /// <summary>
        /// Возможнные состояния экспедиции
        /// </summary>
        public enum ExpeditionStates { NotBusy, Departured, Started, Ended }

        /// <summary>
        /// Текущее состояние экспедиции
        /// </summary>
        public ExpeditionStates CurrentState { get; private set; }
        /// <summary>
        /// Уникальный идентификатор экспедиции
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Дата отправки последней экспедиции
        /// </summary>
        public DateTime? DateDeparture { get; private set; }
        /// <summary>
        /// Дата начала работ последней экспедиции
        /// </summary>
        public DateTime? DateStart { get; private set; }
        /// <summary>
        /// Дата завершения работ последней экспедиции
        /// </summary>
        public DateTime? DateEnd { get; private set; }
        /// <summary>
        /// Дата возврата последней экспедиции 
        /// </summary>
        public DateTime? DateReturn { get; private set; }
        /// <summary>
        /// Исследуемая турбина
        /// </summary>
        public Turbine Turbine { get; private set; }
        /// <summary>
        /// Назначенная бригада
        /// </summary>
        public Brigade Brigade { get; private set; }
        /// <summary>
        /// Бригада утверждена
        /// </summary>
        public bool IsAccepted { get; private set; }
        /// <summary>
        /// Повторяемость вывода
        /// </summary>
        public bool IsRepeated { get; private set; }
        /// <summary>
        /// Период повторения выезда
        /// </summary>
        public TimeSpan Period { get; private set; }
        /// <summary>
        /// Описание последних результатов
        /// </summary>
        public string ExpeditionResult { get; private set; }
        /// <summary>
        /// Набор тестов
        /// </summary>
        public List<ExpeditionTest> Tests { get; private set; }
        /// <summary>
        /// Планируемая дата нчала
        /// </summary>
        public DateTime PlannedStartDate { get; private set; } 

        /// <summary>
        /// Создание выезда
        /// </summary>
        /// <param name="id">Уникальный id экспедиции</param>
        /// <param name="turbine">Место назначения - турбина</param>
        /// <param name="isRepeated">Периодическая или нет </param>
        public Expedition(int id, Turbine turbine, bool isRepeated, DateTime plannedStartDate)
        {
            ID = id;
            Turbine = turbine;
            IsRepeated = isRepeated;
            PlannedStartDate = plannedStartDate;
            Tests = new List<ExpeditionTest>();
            DateDeparture = DateEnd = DateReturn = DateStart = null;
        }

        /// <summary>
        /// Назначение бригады
        /// </summary>
        /// <param name="brigade">Назначенная бригада</param>
        public void SetBrigade(Brigade brigade)
        {
            Brigade = brigade;
        }
        /// <summary>
        /// Утверждение бригады на выезд
        /// </summary>
        public void Accept()
        {
            if (Brigade == null)
                throw new Exception("brigade set yet");
            IsAccepted = true;
        }
        /// <summary>
        /// Переход к следующему состоянию экспедиции
        /// </summary>
        public void NextStep()
        {
            switch(CurrentState)
            {
                case ExpeditionStates.NotBusy:
                    DateDeparture = DateTime.Now;
                    DateEnd = DateReturn = DateStart = null;
                    CurrentState = ExpeditionStates.Departured;
                    break;
                case ExpeditionStates.Departured:
                    DateStart = DateTime.Now;
                    CurrentState = ExpeditionStates.Started;
                    break;
                case ExpeditionStates.Started:
                    DateEnd = DateTime.Now;
                    CurrentState = ExpeditionStates.Ended;
                    break;
                case ExpeditionStates.Ended:
                    DateReturn = DateTime.Now;
                    CurrentState = ExpeditionStates.NotBusy;
                    break;
            }
        }

        /// <summary>
        /// Добавить тесты
        /// </summary>
        /// <param name="test">Новый тест</param>
        public void AddTest(ExpeditionTest test)
        {
            Tests.Add(test);
        }
        
    }
}
