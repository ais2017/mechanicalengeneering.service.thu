﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Директор
    /// </summary>
    public class Director : Person
    {
        /// <summary>
        /// Создание экземпляра директора
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <param name="name">Имя работника</param>
        public Director(int id, string name) : base(id, name) { }
    }
}
