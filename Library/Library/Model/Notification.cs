﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Уведомление о тревоге
    /// </summary>
    public class Notification
    {
        /// <summary>
        /// Возможные состояния уведомления (открытое / закрытое)
        /// </summary>
        public enum NotificationStates { Opened, Closed };
        /// <summary>
        /// Текущее состояние уведомления
        /// </summary>
        public NotificationStates State { get; private set; }
        /// <summary>
        /// Дата создания уведомления
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Назначенная экспедиция (необязательно)
        /// </summary>
        public Expedition Expedition { get; private set; }
        /// <summary>
        /// Турбина, к которой относится уведомление
        /// </summary>
        public Turbine Turbine { get; private set; }
        /// <summary>
        /// Уникальный идентификатор турбины
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Содержание уведомления
        /// </summary>
        public string Definition { get; private set; }

        /// <summary>
        /// Создание уведомления
        /// </summary>
        /// <param name="turbine">Турбина, к которой относится уведомление</param>
        /// <param name="id">Уникальный идентификатор уведомления</param>
        /// <param name="definition">Содержание уведомления</param>
        public Notification(Turbine turbine, int id, string definition)
        {
            Turbine = turbine;
            Date = DateTime.Now;
            State = NotificationStates.Opened;
            ID = id;
            Definition = definition;
            Expedition = null;
        }

        /// <summary>
        /// Закрыть уведомление
        /// </summary>
        public void Close()
        {
            if (State == NotificationStates.Closed)
                throw new Exception("notification alredy closed");
            State = NotificationStates.Closed;
        }

        /// <summary>
        /// Назначить экспедицию (необязательно вызывать)
        /// </summary>
        /// <param name="expedtion">Экспедиция, которая была отправлена по уведомлению</param>
        public void SetExpedition(Expedition expedtion)
        {
            if (State == NotificationStates.Closed)
                throw new Exception("notification alredy closed");
            Expedition = expedtion;
        }
    }
}
