﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Оператор
    /// </summary>
    public class Operator : Person
    {
        /// <summary>
        /// Создание экземпляра оператора
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <param name="name">Имя работника</param>
        Operator(int id, string name) : base(id, name) { }
    }
}
