#Цели разрабатываемой системы
-Сокращение времени обслуживания турбин

#Требования к разрабатываемой системе
1. Система должна содержать следующие группы пользователей
- диспетчеры
- младшие инженеры
- старшие инженеры
- технический директор

2. Система должна:
- предоставлять возможность диспетчерам:
 1)наблюдать за состоянием турбин
 2)назначать ответственную бригаду для экстренного обслуживания
 
- предоставлять возможность старшему инженеру:
 1)просматривать назначенные выезды
 2)просматривать состав своей бригады
 3)изменять состав своей бригады
 4)фиксировать результаты диагностики
 
- предоставлять возможность младшему инженеру:
 1)просматривать назначенные выезды 
 2)фиксировать результаты диагностики
 
- предоставлять возможность техническому директору:
 1)просматривать запросы от старших инженеров на изменение состава брагад
 2)утверждать/отклонять запросы от старших инженеров на изменение состава брагад
 3)просматривать запросы от диспетчеров на выезд бригад
 4)утверждать/отклонять запросы от диспетчеров на выезд бригад
 5)получать уведомления о выезде бригад в случае планового обслуживания
 6)просматривать результаты диагностики

- предоставлять механизм, который будет назначать периодические выезды бригад без участия диспетчеров для планового обслуживания

3. Система должна:
- предоставлять круглосуточный доступ

#Требования к срокам выполнения работ
Начало работ: 23 сентября 2017 года
Дата окончания: 27 декабря 2017 года
